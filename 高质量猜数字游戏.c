//高质量猜数字 

#include <stdio.h>
#include <stdlib.h>
#include <time.h>//time系统时间头文件 
void menu()//创建游戏菜单   void ---不返回值，对return没影响 
{
	printf("**************\n");
	printf("****1.play****\n");
	printf("****0.exit****\n");
	printf("**************\n");
 } 
void playgame()
{
	int magic=rand()%100+1;//随机一个1-100的整数 
	int guess;
	int counter=0;//计数器，初始为0 
	while(1)
	{
		do{
		    printf("请猜一个数字：");//提示用户进行猜数字游戏,输入一个整数
		    scanf("%d",&guess);
		    counter++;
		    if(guess>magic)//根据比较 hh 结果,提示用户高了,低了,或是猜对了.
		    printf("太大了！\n");
		    else if(guess<magic)
		    printf("太小了！\n");
		else
		    printf("right!\n");	 
	    }while(guess!=magic);
	printf("您一共猜了%d次，你真是太棒了！\n",counter);
	}		
}
int main()
{
	int input=0;
	srand((unsigned int)time(0));//time(0)得到的是时间戳,对应头文件#include <time.h>，，Srand是种下随机种子数，你每回种下的种子不一样，用Rand得到的随机数就不一样。为了每回种下一个不一样的种子，所以就选用Time(0)，Time(0)是得到当前时时间值（因为每时每刻时间是不一样的了）。 
	do{
		menu();
	    scanf("%d",&input); 
	    switch(input)
	    {
	    	case 1:
	    		playgame();
	    		break;
	        case 0:
	        	exit(0);
	        	break;
	        default:
	        printf("输入错误！\n");
		}
	}while(input);
	return 0;
}
